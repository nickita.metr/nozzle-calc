import cantera as ct
import math
import numpy as np
from scipy import optimize

def w(lambd, k, R,T0): return lambd*math.sqrt( (2*k/(k+1))*R*T0 )

def tau(lambd, k): return 1-((k-1)/(k+1))*math.pow(lambd,2)
def pi(lambd, k): return math.pow( 1-((k-1)/(k+1))*math.pow(lambd,2) , k/(k-1) )
def q(lambd, k): return np.power( (k+1)/2, 1/(k-1) )*lambd*np.power( 1-((k-1)/(k+1))*np.power(lambd,2) , 1/(k-1) )

def f(lambda_2, k_1, k_2, eps_1, eps_2, lambda_1): return q(lambda_2, k_2) - eps_1*q(lambda_1, k_1)/eps_2

def lambda_max(k): return np.sqrt((k+1)/(k-1))

MPa = math.pow(10,-6)

def equil_properties_mass(gas, dT = 0.1):

    T_0 = gas.T
    p_0 = gas.P
    v_0 = gas.volume_mass
    h_0 = gas.enthalpy_mass
    u_0 = gas.int_energy_mass

    T = T_0 + dT

    gas.TD = T, 1/v_0
    gas.equilibrate('TV')
    u_1 = gas.int_energy_mass
    p_1 = gas.P

    gas.TP = T, p_0
    gas.equilibrate('TP')
    h_2 = gas.enthalpy_mass
    v_1 = gas.volume_mass

    cv = (u_1 - u_0)/dT
    cp = (h_2 - h_0)/dT
    dpdT_v = (p_1 - p_0)/dT
    dvdt_p = (v_1 - v_0)/dT
    k = (v_0/p_0)*(dpdT_v/dvdt_p)*(cp/cv)
    a = math.sqrt(k*p_0*v_0)

    gas.TP = T_0, p_0
    gas.equilibrate('TP')

    result = np.zeros(4)  
    result[:] = [cp, cv, k, a] 
    return result

def isentropic_nozzle(gas, epsilon):

    gas.basis = 'mass'
    h0 = gas.h 
    s0 = gas.s   

    properties = np.zeros(4)
    properties = equil_properties_mass(gas)    
    
    lambda_cc = 0
    cp_cc = properties[0]
    cv_cc = properties[1]
    k_cc = properties[2] 
    a_cc = properties[3]
    R_cc = ct.gas_constant/gas.mean_molecular_weight

    dn = 0.25
    n = math.ceil( 1/dn )
    
    dm = 0.2
    lambda_a = optimize.bisect( f , 1, lambda_max(k_cc)*0.9999, args = (k_cc, k_cc, 1, epsilon, 1) )
    m = math.ceil( (lambda_a - 1)/dm )
    
    print("steps numbers: ", n, m)

    data = np.zeros(( n+m , 12))  # data = [lambda, v, p, T, h, u, k, R, cp, cv, a, epsilon_i]
    data[0, :] = [lambda_cc, gas.v, gas.P, gas.T, h0, gas.u, k_cc, R_cc, cp_cc, cv_cc, a_cc, 0]  

    print("combustion chamber")
    print(gas.report())

    i = 1
    while i <= n:

        lambda_bi = data[i-1,0]        
        p_bi = data[i-1,2]
        k_bi = data[i-1,6]

        lambda_i = lambda_cc + (1 - lambda_cc)*i/n
        
        p0_bi = p_bi/pi(lambda_bi,k_bi)   
        p_i = p0_bi*pi(lambda_i,k_bi)

        gas.SP = s0, p_i
        gas.equilibrate('SP')

        properties = np.zeros(4)        
        properties = equil_properties_mass(gas)     
        cp_i = properties[0]
        cv_i = properties[1]
        k_i = properties[2] 
        a_i = properties[3]  
        R_i = ct.gas_constant/gas.mean_molecular_weight

        data[i, :] = [lambda_i, gas.v, gas.P, gas.T, gas.h, gas.u, k_i, R_i, cp_i, cv_i, a_i, 0]
               
        i += 1
    
    print()
    print("critical section")
    print(gas.report())
    
    data[n,11] = 1

    i = n+1
    while i < n+m:
        
        epsilon_i  = 1+ (epsilon - 1)*math.pow(i-n, 2)/math.pow(m-1, 2)
        
        lambda_bi = data[i-1,0]
        p_bi = data[i-1,2] 
        k_bi = data[i-1,6]
        epsilon_bi = data[i-1,11]

        lambda_i = optimize.bisect( f , 1, lambda_max(k_bi)*0.9999, args = (k_bi, k_bi, epsilon_bi, epsilon_i, lambda_bi) )   
 
        p0_bi = p_bi/pi(lambda_bi,k_bi)   
        p_i = p0_bi*pi(lambda_i,k_bi)

        gas.SP = s0, p_i
        gas.equilibrate('SP')

        properties = np.zeros(4)                
        properties = equil_properties_mass(gas)
        cp_i = properties[0]
        cv_i = properties[1]
        k_i = properties[2] 
        a_i = properties[3]     
        R_i = ct.gas_constant/gas.mean_molecular_weight 

        data[i, :] = [lambda_i, gas.v, gas.P, gas.T, gas.h, gas.u, k_i, R_i, cp_i, cv_i, a_i, epsilon_i]

        i += 1

    print("nozzle exit")
    print(gas.report())

    lambda_cc = data[0,0]
    v_cc = data[0,1]
    p_cc = data[0,2]
    T_cc = data[0,3]
    h_cc = data[0,4]
    u_cc = data[0,5]
    k_cc = data[0,6]
    R_cc = data[0,7]
    cp_cc = data[0,8]
    cv_cc = data[0,9]
    a_cc = data[0,10]

    lambda_cr = data[n,0]
    v_cr = data[n,1]
    p_cr = data[n,2]
    T_cr = data[n,3]
    h_cr = data[n,4]
    u_cr = data[n,5]
    k_cr = data[n,6]
    R_cr = data[n,7]
    cp_cr = data[n,8]
    cv_cr = data[n,9]
    a_cr = data[n,10]

    w_cr = math.sqrt(2*(h_cc-h_cr))
    F_specific_cr = v_cr/w_cr
    J_specific_vacuum_cr = w_cr + p_cr*F_specific_cr    

    lambda_a = data[m+n-1,0]
    v_a = data[m+n-1,1]
    p_a = data[m+n-1,2]
    T_a = data[m+n-1,3]
    h_a = data[m+n-1,4]
    u_a = data[m+n-1,5]
    k_a = data[m+n-1,6]
    R_a = data[m+n-1,7]
    cp_a = data[m+n-1,8]
    cv_a = data[m+n-1,9]
    a_a = data[m+n-1,10]
  
    w_a = math.sqrt(2*(h_cc-h_a))
    F_specific_a = v_a/w_a
    J_specific_vacuum_a = w_a + p_a*F_specific_a

    result = np.zeros((14, 3))
    result[:,0] = [lambda_cc,v_cc,p_cc,T_cc,h_cc,u_cc,k_cc,R_cc,cp_cc,cv_cc,a_cc,0,0,0]
    result[:,1] = [lambda_cr,v_cr,p_cr,T_cr,h_cr,u_cr,k_cr,R_cr,cp_cr,cv_cr,a_cr,w_cr,F_specific_cr,J_specific_vacuum_cr]
    result[:,2] = [lambda_a,v_a,p_a,T_a,h_a,u_a,k_a,R_a,cp_a,cv_a,a_a,w_a,F_specific_a,J_specific_vacuum_a]
    return result

#input section
p_cc = 1/MPa
T_cc = 3000
epsilon = 1.2 
g_air = 44
g_fuel = 1
#input section

kerosene = {'C':7.21, 'H':13.29}
methane = {'C':1, 'H':4}

data_file = "nasa_gas.yaml"
species = ct.Species.list_from_file(data_file)

air = ct.Solution(thermo='ideal-gas', species = species) 
air.X = {'N2':53.916/2, 'O':14.487, 'Ar':0.323, 'C':0.011}
q_air = ct.Quantity(air, mass = g_air)

fuel = ct.Solution(thermo='ideal-gas', species = species) 
fuel.X = kerosene
q_fuel = ct.Quantity(fuel, mass = g_fuel)

gas = q_air + q_fuel

gas.TP = T_cc, p_cc
gas.equilibrate('TP', solver='gibbs')
result = isentropic_nozzle(gas, epsilon) 

print("Result: ")
print()
print(result)
