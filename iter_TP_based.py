import cantera as ct
import math
import numpy as np
from scipy import optimize

def w(lambd, k, R,T0): return lambd*math.sqrt( (2*k/(k+1))*R*T0 )

def tau(lambd, k): return 1-((k-1)/(k+1))*math.pow(lambd,2)
def pi(lambd, k): return math.pow( 1-((k-1)/(k+1))*math.pow(lambd,2) , k/(k-1) )
def q(lambd, k): return np.power( (k+1)/2, 1/(k-1) )*lambd*np.power( 1-((k-1)/(k+1))*np.power(lambd,2) , 1/(k-1) )

def lambda_max(k): return np.sqrt((k+1)/(k-1))

MPa = math.pow(10,-6)


def nozzle(gas, epsilon):

    gas.basis = 'mass'
    h_cc = gas.h
    T_cc = gas.T
    p_cc = gas.P
    v_cc = gas.v
    cp_cc = gas.cp
    
    lambda_cc = 0
    k_cc = gas.cp/gas.cv
    R_cc = ct.gas_constant/gas.mean_molecular_weight    
    
    print("combustion chamber")
    print(gas.report())
    
    p0_cc = p_cc
    T0_cc = T_cc    
    
    lambda_cr = 1
    delta = 1
    k_cr = k_cc
    k_bi = k_cc
    cp_cr = cp_cc
    cp_bi = cp_cc
    while math.fabs(delta) > 0.0001:        
        
        sigma = 1
        p0_cr = p0_cc*sigma
        p_cr = p0_cr*pi(lambda_cr, k_cr)
        
        teta = cp_cc/cp_cr     
        T0_cr = T0_cc*teta                
        T_cr = T0_cr*tau(lambda_cr, k_cr)
        
        gas.TP = T_cr, p_cr
        gas.equilibrate('TP', solver='gibbs')
       
        cp_cr = gas.cp
        k_cr = gas.cp / gas.cv     
        delta = (k_cr - k_bi)/k_cr + (cp_cr - cp_bi)/cp_cr
        k_bi = k_cr 
        cp_bi = cp_cr
     
    print("critical section")
    print(gas.report())   
    R_cr = ct.gas_constant/gas.mean_molecular_weight
    v_cr = gas.v
    h_cr = gas.h
    T_cr = gas.T
    w_cr = gas.sound_speed
   
    def f(lambd, k): return q(lambd, k) - 1/epsilon
    delta = 1
    k_a = k_cr
    k_bi = k_cr
    cp_a = cp_cr
    cp_bi = cp_cr
    while math.fabs(delta) > 0.0001:
        
        lambda_a = optimize.bisect( f , 1, lambda_max(k_a)*0.9999, args = (k_a) )
        
        sigma = 1
        p0_a = p0_cr*sigma
        p_a = p0_a*pi(lambda_a,k_a)
        
        teta = cp_cr/cp_a
        T0_a = T0_cr*teta        
        T_a = T0_a*tau(lambda_a, k_a)
                
        gas.TP = T_a, p_a
        gas.equilibrate('TP', solver='gibbs')
        
        cp_a = gas.cp
        k_a = gas.cp / gas.cv         
        delta = (k_a - k_bi)/k_a + (cp_a - cp_bi)/cp_a
        k_bi = k_a
        cp_bi = cp_a

               
    print("nozzle exit")
    print(gas.report())
    R_a = ct.gas_constant/gas.mean_molecular_weight
    v_a = gas.v
    h_a = gas.h
    T_a = gas.T

    F_specific_cr = v_cr/w_cr
    J_specific_vacuum_cr = w_cr + p_cr*F_specific_cr    

    w_a = lambda_a * w_cr
    F_specific_a = v_a/w_a
    J_specific_vacuum_a = w_a + p_a*F_specific_a

    result = np.zeros((10, 3))
    result[:,0] = [lambda_cc,v_cc,p_cc,T_cc,h_cc,k_cc,R_cc,0,0,0]
    result[:,1] = [lambda_cr,v_cr,p_cr,T_cr,h_cr,k_cr,R_cr,w_cr,F_specific_cr,J_specific_vacuum_cr]
    result[:,2] = [lambda_a,v_a,p_a,T_a,h_a,k_a,R_a,w_a,F_specific_a,J_specific_vacuum_a]
    return result

#input section
p_cc = 1/MPa
T_cc = 3000
epsilon = 36 
g_air = 14
g_fuel = 1
#input section

kerosene = {'C':7.21, 'H':13.29}
methane = {'C':1, 'H':4}

species = ct.Species.list_from_file("nasa_gas.yaml")

air = ct.Solution(thermo='ideal-gas', species = species) 
air.X = {'N':53.916, 'O':14.487, 'Ar':0.323, 'C':0.011}
q_air = ct.Quantity(air, mass = g_air)

fuel = ct.Solution(thermo='ideal-gas', species = species) 
fuel.X = methane
q_fuel = ct.Quantity(fuel, mass = g_fuel)

q_gas = q_air + q_fuel

q_gas.TP = T_cc, p_cc
q_gas.equilibrate('TP', solver='gibbs')

result = nozzle(q_gas, epsilon) 

print("Result: ")
print()
print(result)

