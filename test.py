import cantera as ct
import math
import openpyxl as xl
import nozzle_calc as nc

MPa = math.pow(10,-6)    #коэфф. для удобного пересчета из МПа в Па и обратно

#input section
p_cc = 187219.419
epsilon = 1             #степень геометрического расширения
Km0 = 14
T_H = 223.252
p_H = 26499.873
w_H = 599.063
h_fuel =  -1958.1 * math.pow(10,3)
alpha_start = 1
alpha_step = 0.1
alpha_end = 5
filename = "output.xlsx"       #название файла вывода резуьтата  (только расширение эксель (xlsx))
#input section


data_file = "nasa_gas.yaml"                                     #загружаю вещества из кантеровской встроенной базы данных
species = ct.Species.list_from_file(data_file)

air = ct.Solution(thermo='ideal-gas', species = species)        #создаю воздух
air.X = {'N':53.916, 'O':14.487, 'Ar':0.323, 'C':0.011}         #задаю состав
air.TP = T_H, p_H                                               #задаю параметры воздуха на высоте полета
air.equilibrate('TP')                                           #считаю равновесный соста
h_air = air.h                                                   #считываю статическую энтальпию воздуха
h_0air = h_air + w_H*w_H/2                                      #нахожу полную энтальпию воздуха

fuel = ct.Solution(thermo='ideal-gas', species = species)       #создаю топливо
fuel.X =  {'C':7.21, 'H':13.29}                                 #задаю состав

wb = xl.Workbook()                                                         #создаю таблицу для записи результата
sheet = wb.active                                                          #открываю страницу
subscription = 'Half frozen adiabatic flow. Epsilon case.'                 #заголовок таблицы
sheet.cell(row=1, column = 1).value = subscription                         #записываю заголовок в таблицу
head = ['alpha=', 'T_cc=', 'R_cc=', 'cp=', 'k_cc=']                        #шапка таблицы
l = len(head)                                                              #длинна шапки
for i in range(0,l,1): sheet.cell(row=2, column = i+1).value = head[i]     #записываю шапку в таблицу

alpha = alpha_start
counter = 0
while alpha <= alpha_end:        #цикл, перебиращий значения коэффиуиента избытка оксилителя
    counter += 1

    g_fuel = 1                   #задаю расход топлива
    g_air = Km0*alpha            #считаю расход воздуха
    
    q_air = ct.Quantity(air, mass = g_air)          #создаю газ
    q_fuel = ct.Quantity(fuel, mass = g_fuel)
    q_gas = q_air + q_fuel

    h_cc = (h_0air*g_air + h_fuel*g_fuel)/(g_air + g_fuel)       #полная энтальпия газа
    q_gas.TP = 300, ct.one_atm                                   #инициализирую газ стандартнымы параметрами
    q_gas.equilibrate('TP')          
    q_gas.HP = h_cc , p_cc                                      #задаю параметры газа
    q_gas.equilibrate('HP')                                     #провожу газ к равновесию 

    result = nc.epsilon_half_frozen(q_gas, epsilon)             #вызываю функцию рассчета наполовину замороженного течения

    print("alpha= ", alpha , "h_cc= ", h_cc)                    #вывожу промежуточные результаты      
    print(result)
    print()

    T_cc = result[3,0]              #вывожу требцуемые результаты в таблицу
    R_cc = result[7,0]
    cp_cc = result[8,0]
    k_cc = result[6,0]    
    body = [alpha, T_cc, R_cc, cp_cc, k_cc] 
    l = len(body)
    for i in range(0,l,1): sheet.cell(row = counter + 2, column = i+1).value = body[i]

    alpha += alpha_step

wb.save(filename)          #сохраняю таблицу