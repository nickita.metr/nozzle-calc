import cantera as ct
import math
import openpyxl as xl

MPa = math.pow(10,6)      #коэфф для удобного пересчета из МПа в Па и обратно

#input section
p_mix = 0.14871*MPa
air = {'N':54.761, 'O':14.5567}             #задаю состав воздуха
TS1 =  {'C':7.21, 'H':13.29}                #задаю состав горючего
Km0 = 14.4707
alpha = 3
h_start = -1034.85*1000
h_step = 500*1000
h_end = 2965.15*1000
filename = "output.xlsx"     #название файла вывода резуьтата  (только расширение эксель (xlsx))
#input section


data_file = "nasa_gas.yaml"                      #загружаю вещества из кантеровской встроенной базы данных
species = ct.Species.list_from_file(data_file)
gas = ct.Solution(thermo='ideal-gas', species = species)

wb = xl.Workbook()                                                          #создаю таблицу для записи результата
sheet = wb.active                                                           #открываю страницу 
#subscription = 'Half frozen adiabatic flow. Epsilon case.'                 #заголовок таблицы
#sheet.cell(row=1, column = 1).value = subscription                         #записываю заголовок в таблицу
head = ['alpha=', 'p_mix=', 'T_mix=', 'R_mix=', 'h_mix']                              #шапка таблицы
l = len(head)                                                               #длинна шапки
for i in range(0,l,1): sheet.cell(row=2, column = i+1).value = head[i]      #записываю шапку в таблицу

h = h_start            
counter = 0
while h <= h_end:     #цикл, перебиращий значения коэффициента избытка оксилителя
    counter += 1
 
    Km = alpha*Km0
    print( "alpha= ", alpha, "Km= ", Km,"h=", h)                      
    print()

    gas.set_equivalence_ratio(1/alpha, fuel=TS1, oxidizer=air)      #задаю состав смеси
    gas.TP = 298, ct.one_atm                                        #инициализирую стандартными параметрами
    gas.equilibrate('TP')

    gas.HP = h, p_mix                        #задаю параметры газа
    gas.equilibrate('HP', solver = 'auto')   #привжу газ к равновесию
    #print(gas.report())

    p_mix_r = gas.P
    T_mix_r = gas.T                                       #считываю температуру в КС
    R_mix = ct.gas_constant/gas.mean_molecular_weight   #считываю R смеси 
    h_mix_r = gas.h                                       #считываю h смеси 
  
    body = [alpha, p_mix_r,T_mix_r, R_mix, h_mix_r]                                                            #массив резултьтатов расчета на данном шаге
    l = len(body)                                                                               #длинна массива результатов
    for i in range(0,l,1): sheet.cell(row = counter + 2, column = i+1).value = body[i]          #вывод строки результатов в таблицу

    h += h_step

wb.save(filename)      #сохранение экселевской таблицы