import cantera as ct
import math
import openpyxl as xl

MPa = math.pow(10,6)      #коэфф для удобного пересчета из МПа в Па и обратно

#input section
p_mix = 0.101325*MPa
Km0 = 14.4707
T_mix = 298.15
alpha_start = 1
alpha_step = 0.1
alpha_end = 5
filename = "output.xlsx"     #название файла вывода резуьтата  (только расширение эксель (xlsx))
#input section


data_file = "nasa_gas.yaml"                      #загружаю вещества из кантеровской встроенной базы данных
species = ct.Species.list_from_file(data_file)

air = ct.Solution(thermo='ideal-gas', species = species)   #создаю воздух
air.X = {'N':54.761, 'O':14.5567}                          #задаю состав
air.TP = 298, ct.one_atm                                   #инициализирую воздух стандартными параметрами
air.equilibrate('TP')

fuel = ct.Solution(thermo='ideal-gas', species = species)  #создаю топливо
fuel.X =  {'C':7.21, 'H':13.29}                            #задаю состав
fuel.TP = 298, ct.one_atm                                  #инициализирую горючее стандартнымы параметрами
fuel.equilibrate('TP')

wb = xl.Workbook()                                                          #создаю таблицу для записи результата
sheet = wb.active                                                           #открываю страницу 
#subscription = 'Half frozen adiabatic flow. Epsilon case.'                 #заголовок таблицы
#sheet.cell(row=1, column = 1).value = subscription                         #записываю заголовок в таблицу
head = ['alpha=', 'T_mix=', 'R_mix=', 'h_mix']                              #шапка таблицы
l = len(head)                                                               #длинна шапки
for i in range(0,l,1): sheet.cell(row=2, column = i+1).value = head[i]      #записываю шапку в таблицу

alpha = alpha_start            
counter = 0
while alpha <= alpha_end:     #цикл, перебиращий значения коэффициента избытка оксилителя
    counter += 1
 
    Km = alpha*Km0
    print("alpha=", alpha, "Km= ", Km)            
    #print()

    air.X = {'N':54.761, 'O':14.5567}         #создаю газ в расчетном соотношении (alpha*Km0)
    air.TP = 298, ct.one_atm
    air.equilibrate('TP')
    q_air = ct.Quantity(air, mass = Km)       
    fuel.X =  {'C':7.21, 'H':13.29} 
    fuel.TP = 298, ct.one_atm
    fuel.equilibrate('TP')
    q_fuel = ct.Quantity(fuel, mass = 1)
    q_gas = q_air + q_fuel

    q_gas.TP = T_mix , p_mix                             #задаю параметры газа
    q_gas.equilibrate('TP')                              #привжу газ к равновесию

    T_mix = q_gas.T                                       #считываю температуру в КС
    R_mix = ct.gas_constant/q_gas.mean_molecular_weight   #считываю R смеси 
    h_mix = q_gas.h                                       #считываю h смеси 
  
    body = [alpha, T_mix, R_mix, h_mix]                                                         #массив резултьтатов расчета на данном шаге
    l = len(body)                                                                               #длинна массива результатов
    for i in range(0,l,1): sheet.cell(row = counter + 2, column = i+1).value = body[i]          #вывод строки результатов в таблицу

    alpha += alpha_step

wb.save(filename)      #сохранение экселевской таблицы