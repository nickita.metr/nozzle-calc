import cantera as ct
import math
import openpyxl as xl

MPa = math.pow(10,6)      #коэфф для удобного пересчета из МПа в Па и обратно

#input section
p_cc = 187219.419
Km0 = 14.5636
T_H = 223.252
p_H = 26499.873
w_H = 599.063
h_298_fuel =  -1958.1 * math.pow(10,3)
eta = 0.9
alpha_start = 1
alpha_step = 1
alpha_end = 5
filename = "output.xlsx"     #название файла вывода результата  (только расширение эксель (xlsx))
#input section


data_file = "nasa_gas.yaml"                      #загружаю вещества из кантеровской встроенной базы данных
species = ct.Species.list_from_file(data_file)

air = ct.Solution(thermo='ideal-gas', species = species)   #создаю воздух
air.X = {'N':54.761, 'O':14.5567}                          #задаю состав
air.TP = T_H, p_H                                          #задаю параметры воздуха на высоте полета
air.equilibrate('TP')                                      #считаю равновесный соста
h_air = air.h                                              #считываю статическую энтальпию воздуха
h_0air = h_air + w_H*w_H/2                                 #нахожу полную энтальпию воздуха

fuel = ct.Solution(thermo='ideal-gas', species = species)  #создаю топливо
fuel.X =  {'C':7.21, 'H':13.29}                            #задаю состав
fuel.TP = 298, ct.one_atm                                  #инициализирую горючее стандартнымы параметрами
fuel.equilibrate('TP')

q_air = ct.Quantity(air, mass = Km0)       #создаю газ в стехиометрическом соотношении
q_fuel = ct.Quantity(fuel, mass = 1)
q_gas = q_air + q_fuel
q_gas.TP = 298.15 , p_cc               #задаю параметры продуктов сгорания
q_gas.equilibrate('TP')                #привожу к равновесию
h_298_ps = q_gas.h                     #считываю энтальпию
Hu = h_298_fuel - (1+Km0)*h_298_ps     #считаю теплоту сгорания

wb = xl.Workbook()                                                          #создаю таблицу для записи результата
sheet = wb.active                                                           #открываю страницу 
#subscription = 'Half frozen adiabatic flow. Epsilon case.'                 #заголовок таблицы
#sheet.cell(row=1, column = 1).value = subscription                         #записываю заголовок в таблицу
head = ['alpha=', 'T_cc=', 'R_cc=', 'cp=', 'k_cc=', 'Hu=', "h_cc="]         #шапка таблицы
l = len(head)                                                               #длинна шапки
for i in range(0,l,1): sheet.cell(row=2, column = i+1).value = head[i]      #записываю шапку в таблицу

alpha = alpha_start            
counter = 0
while alpha <= alpha_end:     #цикл, перебиращий значения коэффициента избытка оксилителя
    counter += 1
    
    h_fic = h_0air - Hu*( (1-eta)/(alpha*Km0) )                                     #фиктивная энатльпия воздуха
    h_cc = h_298_fuel*( 1/(1+alpha*Km0) ) + h_fic*( (alpha*Km0)/(1+alpha*Km0) )     #полная энтальпия в камере сгорания

    print("alpha= ", alpha , "Hu= ", Hu, "h_cc= ", h_cc)                      
    print()
    
    q_air = ct.Quantity(air, mass = alpha*Km0)       #создаю газ в расчетном соотношении (alpha*Km0)
    q_fuel = ct.Quantity(fuel, mass = 1)
    q_gas = q_air + q_fuel
    q_gas.HP = h_cc , p_cc           #задаю параметры газа
    q_gas.equilibrate('HP')          #привжу газ к равновесию

    T_cc = q_gas.T                                       #считываю температуру в КС
    R_cc = ct.gas_constant/q_gas.mean_molecular_weight   #считываю R смеси 

    delta_h = h_cc - h_298_ps           #считаю изменение энтальпии

    cp = delta_h/(T_cc - 298.15)       #считаю изобарную теплоемкость процесса
    k = cp/(cp - R_cc)                 #считаю показатель адиабаты процесса
   
    body = [alpha, T_cc, R_cc, cp, k, Hu, h_cc]                                                 #массив резултьтатов расчета на данном шаге
    l = len(body)                                                                               #длинна массива результатов
    for i in range(0,l,1): sheet.cell(row = counter + 2, column = i+1).value = body[i]          #вывод строки результатов в таблицу

    alpha += alpha_step

wb.save(filename)      #сохранение экселевской таблицы