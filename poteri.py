import cantera as ct
import math
import openpyxl as xl
import nozzle_calc as nc

pi = 3.1415926
MPa = math.pow(10,6)      #коэфф для удобного пересчета из МПа в Па и обратно
deg = pi/180

#input section
p_cc = 0.153 *MPa
Km0 = 14.5636
alfa = 3.814
air = {'N':54.761, 'O':14.5567}             #задаю состав воздуха
TS1 =  {'C':7.21, 'H':13.29}                #задаю состав горючего
h_cc =  -11.972 * math.pow(10,3)

eps = 1.66
d_m = 0.85

c_f0 = 0.0045
r = 0.8

P = 23578.584

beta_start = 10 *deg
beta_step = 0.5 *deg
beta_end = 30 *deg

n = 15

filename = "output_poteri.xlsx"     #название файла вывода результата  (только расширение эксель (xlsx))
#input section

data_file = "nasa_gas.yaml"                      #загружаю вещества из кантеровской встроенной базы данных
species = ct.Species.list_from_file(data_file)
gas = ct.Solution(thermo='ideal-gas', species = species)

gas.set_equivalence_ratio(1/alfa, fuel=TS1, oxidizer=air)      #задаю состав смеси
gas.TP = 298, ct.one_atm                                        #инициализирую стандартными параметрами
gas.equilibrate('TP')

F_m = pi*d_m*d_m/4
F_cr = F_m/eps

print("F_m= ", F_m, "F_cr= ", F_cr)

wb = xl.Workbook()                                                          #создаю таблицу для записи результата
sheet = wb.active                                                           #открываю страницу 
subscription = 'Half frozen adiabatic flow. Epsilon case.'                  #заголовок таблицы
sheet.cell(row=1, column = 1).value = subscription                          #записываю заголовок в таблицу
head = ['beta_opt=', 'phi_s=']                                              #шапка таблицы
l = len(head)                                                               #длинна шапки
for i in range(0,l,1): sheet.cell(row=2, column = i+1).value = head[i]      #записываю шапку в таблицу

phi_s_max = 0
beta = beta_start            
counter = 0
while beta <= beta_end:     #цикл, перебиращий значения коэффициента избытка оксилителя
    counter += 1
    
    delta_P = 0
    i = 1
    while i <= n:

        eps_i = 1+(eps - 1)*i/n
        eps_bi = 1+(eps - 1)*(i-1)/n

        gas.HP = h_cc, p_cc                                        #задаю параметры в КС
        gas.equilibrate('HP')

        result = nc.epsilon_half_frozen(gas, eps_i)             #вызываю функцию рассчета наполовину замороженного течения
        lambda_a = result[0,2]
        k = result[6,2]
        M = math.sqrt( ( 2*lambda_a*lambda_a/(k+1) )/( 1 - (k-1)*lambda_a*lambda_a/(k+1) ) )
        w = result[11,2]
        v = result[1,2]
        rho = 1/v

        c_f = c_f0*math.pow(1+r*(k-1)*M*M/2, -0.55)

        F_i = F_cr*eps_i
        F_bi = F_cr*eps_bi
        S = (F_i - F_bi)/math.sin(beta)
        

        delta_P_i = c_f*rho*w*w*S*math.cos(beta)
        delta_P = delta_P + delta_P_i

        print("i= ", i, "eps= ", eps_i, "k= ", k, "M= ", M, "w= ", w, "v= ", v, "c_f= ", c_f, "S= ", S, "delta_P_i", delta_P_i)
        i += 1 

    phi_tr = 1 - delta_P/P
    phi_r = (1 + math.cos(beta))/2
    phi_s = phi_tr*phi_r
    
    print()
    print("beta= ", beta/deg,"delta_P= ", delta_P, "phi_tr= ", phi_tr,"phi_s= ", phi_s)

    if phi_s >= phi_s_max:
        phi_s_max = phi_s
        beta_opt = beta
    
    body = [beta, phi_s]                                                                        #массив резултьтатов расчета на данном шаге
    l = len(body)                                                                               #длинна массива результатов
    for i in range(0,l,1): sheet.cell(row = counter + 2, column = i+1).value = body[i]          #вывод строки результатов в таблицу

    beta += beta_step

sheet.cell(row=3, column = 3).value = "beta_opt="
sheet.cell(row=3, column = 4).value = beta_opt
sheet.cell(row=4, column = 3).value = "phi_s_max="
sheet.cell(row=4, column = 4).value = phi_s_max
wb.save(filename)                                             #сохранение экселевской таблицы