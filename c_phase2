import cantera as ct
import math
import numpy as np
from scipy import optimize

def w(lambd, k, R,T0): return lambd*math.sqrt( (2*k/(k+1))*R*T0 )

def tau(lambd, k): return 1-((k-1)/(k+1))*math.pow(lambd,2)
def pi(lambd, k): return math.pow( 1-((k-1)/(k+1))*math.pow(lambd,2) , k/(k-1) )
def q(lambd, k): return np.power( (k+1)/2, 1/(k-1) )*lambd*np.power( 1-((k-1)/(k+1))*np.power(lambd,2) , 1/(k-1) )

def f(lambda_2, k_1, k_2, eps_1, eps_2, lambda_1): return q(lambda_2, k_2) - eps_1*q(lambda_1, k_1)/eps_2

def lambda_max(k): return np.sqrt((k+1)/(k-1))

MPa = math.pow(10,-6)


def equil_properties_mass(gas, dT = 0.01):

    T_0 = gas.T
    p_0 = gas.P
    v_0 = gas.volume_mass
    h_0 = gas.enthalpy_mass
    u_0 = gas.int_energy_mass

    T = T_0 + dT

    gas.TD = T, 1/v_0
    gas.equilibrate('TV')
    u_1 = gas.int_energy_mass
    p_1 = gas.P

    gas.TP = T, p_0
    gas.equilibrate('TP')
    h_2 = gas.enthalpy_mass
    v_1 = gas.volume_mass

    cv = (u_1 - u_0)/dT
    cp = (h_2 - h_0)/dT
    dpdT_v = (p_1 - p_0)/dT
    dvdt_p = (v_1 - v_0)/dT
    k = (v_0/p_0)*(dpdT_v/dvdt_p)*(cp/cv)
    a = math.sqrt(k*p_0*v_0)

    gas.TP = T_0, p_0
    gas.equilibrate('TP')

    result = np.zeros(4)  
    result[:] = [cp, cv, k, a] 
    return result

def isentropic_nozzle(gas, epsilon):

    gas.basis = 'mass'
    delta_max = 0.0001

    print()
    print("combustion chamber:")
    print(gas.report())
     
    s0 = gas.s 

    v_cc = gas.v    
    p_cc = gas.P
    T_cc = gas.T
    h_cc = gas.h 
    u_cc = gas.u 

    properties = np.zeros(4)
    properties = equil_properties_mass(gas)    
    
    lambda_cc = 0
    cp_cc = properties[0]
    cv_cc = properties[1]
    k_cc = properties[2] 
    a_cc = properties[3]
    R_cc = ct.gas_constant/gas.mean_molecular_weight
    
    delta = 1
    k_cr = k_cc
    k_bi = k_cc
    cp_cr = cp_cc
    cp_bi = cp_cc
    a_cr = a_cc
    a_bi = a_cc
    while math.fabs(delta) > delta_max:
        
        p_cr = p_cc*math.pow( 2/(k_cr+1), k_cr/(k_cr-1) )
        
        gas.SP = s0, p_cr
        gas.equilibrate('SP')
        
        properties = np.zeros(4)
        properties = equil_properties_mass(gas)        
       
        cp_cr = properties[0]
        cv_cr = properties[1]
        k_cr = properties[2]
        a_cr = properties[3]

                      
        delta = (k_cr - k_bi)/k_cr + (cp_cr - cp_bi)/cp_cr + (a_cr - a_bi)/a_cr
        print("delta_sub= ",delta)
        k_bi = k_cr 
        cp_bi = cp_cr
        a_bi = a_cr

    lambda_cr = 1   
    print("critical section:")
    print(gas.report()) 

    R_cr = ct.gas_constant/gas.mean_molecular_weight
    v_cr = gas.v    
    T_cr = gas.T
    h_cr = gas.h
    u_cr = gas.u
    w_cr = a_cr
 
    delta = 1 
    k_a = k_cr
    k_bi = k_cr
    cp_a = cp_cr
    cp_bi = cp_cr
    a_a = a_cr
    a_bi = a_cr      
    while math.fabs(delta) > delta_max:
        
        lambda_a = optimize.bisect( f , 1, lambda_max(k_a)*0.9999, args = (k_cr, k_a, 1, epsilon, lambda_cr) )
        p_a = p_cc*pi(lambda_a,k_a)
                
        gas.SP = s0, p_a
        gas.equilibrate('SP')

        properties = np.zeros(4)
        properties = equil_properties_mass(gas)        
       
        cp_a = properties[0]
        cv_a = properties[1]
        k_a = properties[2]  
        a_a = properties[3]

        delta = (k_a - k_bi)/k_a + (cp_a - cp_bi)/cp_a + (a_a - a_bi)/a_a
        print("delta_super= ",delta)
        k_bi = k_a 
        cp_bi = cp_a
        a_bi = a_a
               
    print("nozzle exit:")
    print(gas.report())
    R_a = ct.gas_constant/gas.mean_molecular_weight
    v_a = gas.v
    T_a = gas.T
    h_a = gas.h
    u_a = gas.u
    
    w_cr = math.sqrt(2*(h_cc-h_cr))
    F_specific_cr = v_cr/w_cr
    J_specific_vacuum_cr = w_cr + p_cr*F_specific_cr    

    w_a = math.sqrt(2*(h_cc-h_a))
    F_specific_a = v_a/w_a
    J_specific_vacuum_a = w_a + p_a*F_specific_a

    result = np.zeros((14, 3))
    result[:,0] = [lambda_cc,v_cc,p_cc,T_cc,h_cc,u_cc,k_cc,R_cc,cp_cc,cv_cc,a_cc,0,0,0]
    result[:,1] = [lambda_cr,v_cr,p_cr,T_cr,h_cr,u_cr,k_cr,R_cr,cp_cr,cv_cr,a_cr,w_cr,F_specific_cr,J_specific_vacuum_cr]
    result[:,2] = [lambda_a,v_a,p_a,T_a,h_a,u_a,k_a,R_a,cp_a,cv_a,a_a,w_a,F_specific_a,J_specific_vacuum_a]
    return result

#input section
p_cc = 0.1/MPa
epsilon = 1.2 
g_air = 0
T_H = 300
p_H = 0.01/MPa
g_fuel = 1
h_fuel = -1958.1 * pow(10,3)
#input section

kerosene = {'C':7.21, 'H':13.29}
methane = {'C':1, 'H':4}

gas_spec = ct.Species.list_from_file("nasa_gas.yaml")
gas = ct.Solution(thermo='ideal-gas', species = gas_spec)
c_phase = ct.Solution("graphite.yaml")
phases = [(gas, 1.0), (c_phase, 0.0)]

mix = ct.Mixture(phases)

mix.T = 2277
mix.P = p_cc
mix.species_moles = "H:88.61, C:48.17, N:503.21, O:135.21, Ar:3.01"
mix.equilibrate('TP')

print(mix.report())