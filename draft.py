import cantera as ct
import math
import numpy as np
from scipy import optimize

def w(lambd, k, R,T0): return lambd*math.sqrt( (2*k/(k+1))*R*T0 )

def tau(lambd, k): return 1-((k-1)/(k+1))*math.pow(lambd,2)
def pi(lambd, k): return math.pow( 1-((k-1)/(k+1))*math.pow(lambd,2) , k/(k-1) )
def q(lambd, k): return np.power( (k+1)/2, 1/(k-1) )*lambd*np.power( 1-((k-1)/(k+1))*np.power(lambd,2) , 1/(k-1) )

def lambda_max(k): return np.sqrt((k+1)/(k-1))

MPa = math.pow(10,-6)

def isentropic_nozzle(gas, epsilon):

    gas.basis = 'mass'
    s0 = gas.s
    h0 = gas.h    

    n = 10
    m = 50

    data = np.zeros(( n+m , 8))  # data = [lambda, v, p, T, h, k, R, epsilon_i]
    
    lambda_cc = 0
    k_cc = gas.cp/gas.cv
    R_cc = ct.gas_constant/gas.mean_molecular_weight
    cp_cc = gas.cp
    w_cc = 0
    data[0, :] = [lambda_cc, gas.v, gas.P, gas.T, h0, k_cc, R_cc, 0]

    print()
    print("combustion chamber")
    gas()

    i = 1
    while i <= n:
        #print('Progress: ', i*100/(m+n-1), '%')

        lambda_bi = data[i-1,0]        
        p_bi = data[i-1,2]
        k_bi = data[i-1,5]

        lambda_i = lambda_cc + (1 - lambda_cc)*i/n
        
        p0_i = p_bi/pi(lambda_bi,k_bi)
        p_i = p0_i*pi(lambda_i,k_bi)
                
        gas.SP = s0, p_i
        gas.equilibrate('SP')
        
        k_i = gas.cp / gas.cv
        R_i = ct.gas_constant/gas.mean_molecular_weight

        data[i, :] = [lambda_i, gas.v, p_i, gas.T, gas.h, k_i, R_i, 0]
               
        i += 1
    
    print("critical section")
    gas()
    
    data[n,7] = 1

    i = n+1
    while i < n+m:
        #print('Progress: ', i*100/(m+n-1), '%')
        
        epsilon_i  = 1+ (epsilon - 1)*math.pow(i-n, 2)/math.pow(m-1, 2)
        
        k_bi = data[i-1,5]
        lambda_bi = data[i-1,0]
        p_bi = data[i-1,2] 
        epsilon_bi = data[i-1,7]
                
        def f(lambda_i): return q(lambda_i, k_bi) - epsilon_bi*q(lambda_bi, k_bi)/epsilon_i
        lambda_i = optimize.bisect( f , 1, lambda_max(k_bi)*0.9999)
 
        p0_i = p_bi/pi(lambda_bi,k_bi)
        p_i = p0_i*pi(lambda_i,k_bi)
        
        gas.SP = s0, p_i
        gas.equilibrate('SP')
        
        k_i = gas.cp / gas.cv
        R_i = ct.gas_constant/gas.mean_molecular_weight 

        data[i, :] = [lambda_i, gas.v, p_i, gas.T, gas.h, k_i, R_i, epsilon_i]

        i += 1

    print("nozzle exit")
    gas()

    lambda_cc = data[0,0]
    v_cc = data[0,1]
    p_cc = data[0,2]
    T_cc = data[0,3]
    h_cc = data[0,4]
    k_cc = data[0,5]
    R_cc = data[0,6]

    lambda_cr = data[n,0]
    v_cr = data[n,1]
    p_cr = data[n,2]
    T_cr = data[n,3]
    h_cr = data[n,4]
    k_cr = data[n,5]
    R_cr = data[n,6]

    T0_cr = T_cr/tau(lambda_cr,k_cr)
    w_cr = lambda_cr * math.sqrt(k_cr*R_cr*T_cr)

    F_specific_cr = v_cr/w_cr
    J_specific_vacuum_cr = w_cr + p_cr*F_specific_cr    

    lambda_a = data[m+n-1,0]
    v_a = data[m+n-1,1]
    p_a = data[m+n-1,2]
    T_a = data[m+n-1,3]
    h_a = data[m+n-1,4]
    k_a = data[m+n-1,5]
    R_a = data[m+n-1,6]

    T0_a = T_a/tau(lambda_a,k_a)
    w_a = lambda_a * w_cr

    F_specific_a = v_a/w_a
    J_specific_vacuum_a = w_a + p_a*F_specific_a

    result = np.zeros((10, 3))
    result[:,0] = [lambda_cc,v_cc,p_cc,T_cc,h_cc,k_cc,R_cc,0,0,0]
    result[:,1] = [lambda_cr,v_cr,p_cr,T_cr,h_cr,k_cr,R_cr,w_cr,F_specific_cr,J_specific_vacuum_cr]
    result[:,2] = [lambda_a,v_a,p_a,T_a,h_a,k_a,R_a,w_a,F_specific_a,J_specific_vacuum_a]
    return result
   
species1 = ct.Species.list_from_file("nasa_gas.yaml")
species2 = ct.Species.list_from_file("nasa_condensed.yaml")
gas = ct.Solution(thermo='ideal-gas', species = species1) 

c_species = ct.Species.list_from_file("graphite.yaml")
c_phase = ct.Solution(thermo='fixed-stoichiometry', species = c_species)

gas_gri30 = ct.Solution("gri30.yaml") 

air = "N:53.916, O:14.487, Ar:0.323, C:0.011"
fuel = "C:7.21, H:13.29"
gas_gri30.set_equivalence_ratio(phi=1, fuel=fuel, oxidizer=air, basis='mole')
gas_gri30.TP = 3000, 10.0/MPa
gas_gri30.equilibrate('TP')

#gas.TP = 3000, 10.0/MPa
#gas.TPY = 3000, 10.0/MPa, 'H2:1, O2:7.9370'

#gas.TPX = 3000, 10.0/MPa, 'N:1, H:4, Cl:1, O:4' #amonia perchlorate 
#gas.TPX = 3000, 10.0/MPa, 'N:1, Cl:1, O:6' #nitronium perchlorate
#gas.TPX = 3000, 10.0/MPa, 'N:4.95, H:90.6, O:3.89, C:64.8' #SKN


gas.equilibrate('TP')   

epsilon = 36
result = isentropic_nozzle(gas_gri30, epsilon) 

print("Result: ")
print()
print(result)

