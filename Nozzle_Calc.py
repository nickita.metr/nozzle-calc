import cantera as ct
import math                  
import numpy as np
from scipy import optimize

def tau(lambd, k): return 1-((k-1)/(k+1))*math.pow(lambd,2)  #ГДФ температуры
def pi(lambd, k): return math.pow( 1-((k-1)/(k+1))*math.pow(lambd,2) , k/(k-1) )  #ГДФ давления
def q(lambd, k): return np.power( (k+1)/2, 1/(k-1) )*lambd*np.power( 1-((k-1)/(k+1))*np.power(lambd,2) , 1/(k-1) )  #ГДФ расхода

def f(lambda_2, k_2, eps_2): return q(lambda_2, k_2) - 1/eps_2  #необходимо для рассчета приведенной скорости в сечении через степень геометрического расширения (следует из ур-ия расхода)

def lambda_max(k): return np.sqrt((k+1)/(k-1)) #максимальная приведенная скорость при данных параметров газа 

MPa = math.pow(10,-6)   #коэфф для удобного пересчета из МПа в Па и обратно

def equil_properties_mass(gas, dT = 0.1):  #функция рассчитывающая равновесные параметры газа

    T_0 = gas.T                     #считываю изначальные параметры газа
    p_0 = gas.P
    v_0 = gas.volume_mass
    h_0 = gas.enthalpy_mass
    u_0 = gas.int_energy_mass

    T = T_0 + dT                   #варьирую температуру на заданную величину

    gas.TD = T, 1/v_0              #задаю газу нувые параметры (постоянный объем)
    gas.equilibrate('TV')          
    u_1 = gas.int_energy_mass      #считываю параметры газа в новом состоянии   
    p_1 = gas.P

    gas.TP = T, p_0                #задаю газу нувые параметры (постоянное давление)
    gas.equilibrate('TP')          #привожу газ к равновесному составу
    h_2 = gas.enthalpy_mass        #считываю параметры газа в новом состоянии
    v_1 = gas.volume_mass

    cv = (u_1 - u_0)/dT            #расчет свойств газа
    cp = (h_2 - h_0)/dT
    dpdT_v = (p_1 - p_0)/dT
    dvdt_p = (v_1 - v_0)/dT
    k = (v_0/p_0)*(dpdT_v/dvdt_p)*(cp/cv)
    a = math.sqrt(k*p_0*v_0)

    gas.TP = T_0, p_0              #привожу газ к изначальному состоянию
    gas.equilibrate('TP')

    result = np.zeros(4)  
    result[:] = [cp, cv, k, a]     #вывожу результаты
    return result

def epsilon_half_frozen(gas, epsilon):   #течение равновесное до критики и замороженное после критики

    gas.basis = 'mass'             #на всякий случай указываю кантере что я работаю с массовыми а не мольными долями, работает и без этого
     
    s0 = gas.s        #сохраняю энтропию 

    v_cc = gas.v      #считываю  параметры газа в КС
    p_cc = gas.P
    T_cc = gas.T
    h_cc = gas.h 
    u_cc = gas.u 

    properties = np.zeros(4)                       #определяю свойста газа в КС
    properties = equil_properties_mass(gas)    
    
    lambda_cc = 0                          #приведенная скорость в КС равна нулю
    cp_cc = properties[0]
    cv_cc = properties[1]
    k_cc = properties[2] 
    a_cc = properties[3]
    R_cc = ct.gas_constant/gas.mean_molecular_weight
    
    #print("combustion chamber")
    #print(gas.report())
    
    delta = 1           #невязка
    k_cr = k_cc         #начальные приближения для итерационного расчета
    k_bi = k_cc
    cp_cr = cp_cc
    cp_bi = cp_cc
    a_cr = a_cc
    a_bi = a_cc
    while math.fabs(delta) > 0.0001:   #итерационный расчет, считаю, пока невяка не станет меньше заданного значения
        
        p_cr = p_cc*math.pow( 2/(k_cr+1), k_cr/(k_cr-1) )       #рассчитываю давление исходя из постоянства полного давления
        
        gas.SP = s0, p_cr            #задаю параметры газа с помощью давления и энтропии (процесс считаю изоэнтропным)
        gas.equilibrate('SP')        #привожу газ к равновесию
        
        properties = np.zeros(4)                     #рассчитываю свойства газа
        properties = equil_properties_mass(gas)        
       
        cp_cr = properties[0]
        cv_cr = properties[1]
        k_cr = properties[2]
        a_cr = properties[3]

                      
        delta = (k_cr - k_bi)/k_cr + (cp_cr - cp_bi)/cp_cr + (a_cr - a_bi)/a_cr       #невязка 
        #print("delta_sub= ",delta)
        k_bi = k_cr                    #присваиваю новые значения переменным сравнения
        cp_bi = cp_cr
        a_bi = a_cr

    lambda_cr = 1                       #приведенная скорость в критике равна еденице

    #print("critical section")
    #print(gas.report()) 

    R_cr = ct.gas_constant/gas.mean_molecular_weight       #рассчитываю свойства газа в критике
    v_cr = gas.v    
    T_cr = gas.T
    h_cr = gas.h
    u_cr = gas.u
    w_cr = a_cr
    k_cr_frozen = gas.cp / gas.cv
 
    if epsilon == 1:                    #если степень геометрического расширения равна еденице, то дальнейший рассчет не требуется, выходное сечение тождественно критическому
        lambda_a = lambda_cr
        k_a = k_cr_frozen
        cp_a = gas.cp
        cv_a = gas.cv
        R_a = R_cr  
        v_a = v_cr
        h_a = h_cr
        u_a = u_cr
        T_a = T_cr
        p_a = p_cr
        a_a = math.sqrt(k_a*R_a*T_a)

    else:                               #если нет, то считается выходное сечение
        delta = 1                       #невязка
        k_a = k_cr_frozen               #начальные приближения для итерационного расчета
        k_bi = k_cr_frozen
        cp_a = gas.cp
        cp_bi = gas.cv   
        while math.fabs(delta) > 0.0001:    #итерационный расчет, считаю, пока невяка не станет меньше заданного значения         
            
            lambda_a = optimize.bisect( f , 1, lambda_max(k_a)*0.9999, args = (k_a, epsilon) )  #рассчитываю приведенную скорость с помощью ГДФ расхода (следует из ур-ия расхода)

            p_a = p_cc*pi(lambda_a,k_a)    #рассчитываю давление исходя из постоянства полного давления    
            gas.SP = s0, p_a               #задаю параметры газа с помощью давления и энтропии (процесс считаю изоэнтропным) 
        
            cp_a = gas.cp                  #рассчитываю свойства газа
            cv_a = gas.cv
            k_a = gas.cp / gas.cv

            delta = (k_a - k_bi)/k_a + (cp_a - cp_bi)/cp_a      #невязка
            #print("delta_super= ",delta)
            k_bi = k_a                        #присваиваю новые значения переменным сравнения
            cp_bi = cp_a
                    
        #print("nozzle exit")
        #print(gas.report())  

        R_a = ct.gas_constant/gas.mean_molecular_weight       #рассчитываю свойства газа на срезе сопла
        v_a = gas.v
        h_a = gas.h
        u_a = gas.u
        T_a = gas.T
        a_a = math.sqrt(k_a*R_a*T_a)
    
    w_cr = math.sqrt(2*(h_cc-h_cr))                     #рассчитываю скорость потока (h0 = h + w2/2)
    F_specific_cr = v_cr/w_cr                           #удельная площадь критики
    J_specific_vacuum_cr = w_cr + p_cr*F_specific_cr    #пустотный удельный импульс

    w_a = math.sqrt(2*(h_cc-h_a))                       #рассчитываю скорость потока (h0 = h + w2/2)
    F_specific_a = v_a/w_a                              #удельная площадь среза сопла
    J_specific_vacuum_a = w_a + p_a*F_specific_a        #пустотный удельный импульс

    result = np.zeros((14, 3))                          #собираю результаты расета в двумерный массив
    result[:,0] = [lambda_cc,v_cc,p_cc,T_cc,h_cc,u_cc,k_cc,R_cc,cp_cc,cv_cc,a_cc,0,0,0]
    result[:,1] = [lambda_cr,v_cr,p_cr,T_cr,h_cr,u_cr,k_cr,R_cr,cp_cr,cv_cr,a_cr,w_cr,F_specific_cr,J_specific_vacuum_cr]
    result[:,2] = [lambda_a,v_a,p_a,T_a,h_a,u_a,k_a,R_a,cp_a,cv_a,a_a,w_a,F_specific_a,J_specific_vacuum_a]
    return result

def epsilon_equil(gas, epsilon):     #течение равновесное

    gas.basis = 'mass'          #на всякий случай указываю кантере что я работаю с массовыми а не мольными долями, работает и без этого
     
    s0 = gas.s                  #сохраняю энтропию 

    v_cc = gas.v                #считываю  параметры газа в КС
    p_cc = gas.P
    T_cc = gas.T
    h_cc = gas.h 
    u_cc = gas.u 

    properties = np.zeros(4)                     #определяю свойста газа в КС
    properties = equil_properties_mass(gas)    
    
    lambda_cc = 0
    cp_cc = properties[0]
    cv_cc = properties[1]
    k_cc = properties[2] 
    a_cc = properties[3]
    R_cc = ct.gas_constant/gas.mean_molecular_weight
    
    #print("combustion chamber")
    #print(gas.report())
    
    delta = 1                        #невязка
    k_cr = k_cc                      #начальные приближения для итерационного расчета
    k_bi = k_cc
    cp_cr = cp_cc
    cp_bi = cp_cc
    a_cr = a_cc
    a_bi = a_cc
    while math.fabs(delta) > 0.0001:       #итерационный расчет, считаю, пока невяка не станет меньше заданного значения
        
        p_cr = p_cc*math.pow( 2/(k_cr+1), k_cr/(k_cr-1) )    #рассчитываю давление исходя из постоянства полного давления
        
        gas.SP = s0, p_cr                      #задаю параметры газа с помощью давления и энтропии (процесс считаю изоэнтропным)
        gas.equilibrate('SP')                  #привожу газ к равновесию
        
        properties = np.zeros(4)
        properties = equil_properties_mass(gas)        
       
        cp_cr = properties[0]
        cv_cr = properties[1]
        k_cr = properties[2]
        a_cr = properties[3]

                      
        delta = (k_cr - k_bi)/k_cr + (cp_cr - cp_bi)/cp_cr + (a_cr - a_bi)/a_cr     #невязка
        #print("delta_sub= ",delta)
        k_bi = k_cr                      #присваиваю новые значения переменным сравнения
        cp_bi = cp_cr
        a_bi = a_cr

    lambda_cr = 1                       #приведенная скорость в критике равна еденице

    #print("critical section")
    #print(gas.report()) 

    R_cr = ct.gas_constant/gas.mean_molecular_weight       #рассчитываю свойства газа в критике
    v_cr = gas.v    
    T_cr = gas.T
    h_cr = gas.h
    u_cr = gas.u
    w_cr = a_cr
 
    if epsilon == 1:                       #если степень геометрического расширения равна еденице, то дальнейший рассчет не требуется, выходное сечение тождественно критическому
        lambda_a = lambda_cr
        k_a = k_cr
        cp_a = cp_cr
        cv_a = cv_cr
        R_a = R_cr  
        v_a = v_cr
        h_a = h_cr
        u_a = u_cr
        T_a = T_cr
        p_a = p_cr
        a_a = a_cr
    else:                  #если нет, то считается выходное сечение
        delta = 1          #невязка
        k_a = k_cr         #начальные приближения для итерационного расчета
        k_bi = k_cr
        cp_a = cp_cr
        cp_bi = cp_cr
        a_a = a_cr
        a_bi = a_cr      
        while math.fabs(delta) > 0.0001:      #итерационный расчет, считаю, пока невяка не станет меньше заданного значения
            
            lambda_a = optimize.bisect( f , 1, lambda_max(k_a)*0.9999, args = ( k_a, epsilon) )

            p_a = p_cc*pi(lambda_a,k_a)       #рассчитываю давление исходя из постоянства полного давления
                    
            gas.SP = s0, p_a                  #задаю параметры газа с помощью давления и энтропии (процесс считаю изоэнтропным)
            gas.equilibrate('SP')             #привожу газ к равновесию
            
            properties = np.zeros(4)
            properties = equil_properties_mass(gas)        
        
            cp_a = properties[0]
            cv_a = properties[1]
            k_a = properties[2]  
            a_a = properties[3]

            delta = (k_a - k_bi)/k_a + (cp_a - cp_bi)/cp_a + (a_a - a_bi)/a_a            #невязка
            #print("delta_super= ",delta)
            k_bi = k_a                       #присваиваю новые значения переменным сравнения
            cp_bi = cp_a
            a_bi = a_a
                
        #print("nozzle exit")
        #print(gas.report())

        R_a = ct.gas_constant/gas.mean_molecular_weight         #рассчитываю свойства газа на срезе сопла
        v_a = gas.v
        T_a = gas.T
        h_a = gas.h
        u_a = gas.u
    
    w_cr = math.sqrt(2*(h_cc-h_cr))                            #рассчитываю скорость потока (h0 = h + w2/2)
    F_specific_cr = v_cr/w_cr                                  #удельная площадь критики
    J_specific_vacuum_cr = w_cr + p_cr*F_specific_cr           #пустотный удельный импульс

    w_a = math.sqrt(2*(h_cc-h_a))                              #рассчитываю скорость потока (h0 = h + w2/2)
    F_specific_a = v_a/w_a                                     #удельная площадь среза сопла
    J_specific_vacuum_a = w_a + p_a*F_specific_a               #пустотный удельный импульс

    result = np.zeros((14, 3))                                 #собираю результаты расета в двумерный массив
    result[:,0] = [lambda_cc,v_cc,p_cc,T_cc,h_cc,u_cc,k_cc,R_cc,cp_cc,cv_cc,a_cc,0,0,0]
    result[:,1] = [lambda_cr,v_cr,p_cr,T_cr,h_cr,u_cr,k_cr,R_cr,cp_cr,cv_cr,a_cr,w_cr,F_specific_cr,J_specific_vacuum_cr]
    result[:,2] = [lambda_a,v_a,p_a,T_a,h_a,u_a,k_a,R_a,cp_a,cv_a,a_a,w_a,F_specific_a,J_specific_vacuum_a]
    return result

def epsilon_frozen(gas, epsilon):       #течение замороженное

    gas.basis = 'mass'                  #на всякий случай указываю кантере что я работаю с массовыми а не мольными долями, работает и без этого
     
    s0 = gas.s                          #сохраняю энтропию 

    v_cc = gas.v                        #считываю  параметры газа в КС
    p_cc = gas.P
    T_cc = gas.T
    h_cc = gas.h 
    u_cc = gas.u 

    properties = np.zeros(4)                       #определяю свойста газа в КС
    properties = equil_properties_mass(gas)    
    
    lambda_cc = 0                 #приведенная скорость в КС равна нулю
    cp_cc = properties[0]
    cv_cc = properties[1]
    k_cc = properties[2] 
    a_cc = properties[3]
    R_cc = ct.gas_constant/gas.mean_molecular_weight
    
    #print("combustion chamber")
    #print(gas.report())
    
    delta = 1                         #невязка
    k_cr = gas.cp / gas.cv            #начальные приближения для итерационного расчета
    k_bi = gas.cp / gas.cv
    cp_cr = gas.cp
    cp_bi = gas.cp
    while math.fabs(delta) > 0.0001:              #итерационный расчет, считаю, пока невяка не станет меньше заданного значения
        
        p_cr = p_cc*math.pow( 2/(k_cr+1), k_cr/(k_cr-1) )       #рассчитываю давление исходя из постоянства полного давления
        
        gas.SP = s0, p_cr               #задаю параметры газа с помощью давления и энтропии (процесс считаю изоэнтропным)

        cp_cr = gas.cp
        cv_cr = gas.cv
        k_cr = gas.cp / gas.cv
                      
        delta = (k_cr - k_bi)/k_cr + (cp_cr - cp_bi)/cp_cr        #невязка
        #print("delta_sub= ",delta)
        k_bi = k_cr                     #присваиваю новые значения переменным сравнения
        cp_bi = cp_cr
   
    lambda_cr = 1                      #приведенная скорость в критике равна еденице

    #print("critical section")
    #print(gas.report()) 

    R_cr = ct.gas_constant/gas.mean_molecular_weight        #рассчитываю свойства газа в критике
    v_cr = gas.v    
    T_cr = gas.T
    h_cr = gas.h
    u_cr = gas.u
    a_cr = math.sqrt(k_cr*R_cr*T_cr)
 
    if epsilon == 1:                                  #если степень геометрического расширения равна еденице, то дальнейший рассчет не требуется, выходное сечение тождественно критическому
        lambda_a = lambda_cr
        k_a = k_cr
        cp_a = gas.cp
        cv_a = gas.cv
        R_a = R_cr  
        v_a = v_cr
        h_a = h_cr
        u_a = u_cr
        T_a = T_cr
        p_a = p_cr
        a_a = math.sqrt(k_a*R_a*T_a)

    else:                   #если нет, то считается выходное сечение
        delta = 1           #невязка
        k_a = k_cr          #начальные приближения для итерационного расчета
        k_bi = k_cr
        cp_a = gas.cp
        cp_bi = gas.cv   
        while math.fabs(delta) > 0.0001:                #итерационный расчет, считаю, пока невяка не станет меньше заданного значения  
            
            lambda_a = optimize.bisect( f , 1, lambda_max(k_a)*0.9999, args = (k_a, epsilon) )

            p_a = p_cc*pi(lambda_a,k_a)      #рассчитываю давление исходя из постоянства полного давления

            gas.SP = s0, p_a                 #задаю параметры газа с помощью давления и энтропии (процесс считаю изоэнтропным)
        
            cp_a = gas.cp
            cv_a = gas.cv
            k_a = gas.cp / gas.cv

            delta = (k_a - k_bi)/k_a + (cp_a - cp_bi)/cp_a           #невязка
            #print("delta_super= ",delta)
            k_bi = k_a                       #присваиваю новые значения переменным сравнения
            cp_bi = cp_a
                    
        #print("nozzle exit")
        #print(gas.report())  

        R_a = ct.gas_constant/gas.mean_molecular_weight           #рассчитываю свойства газа на срезе сопла
        v_a = gas.v
        h_a = gas.h
        u_a = gas.u
        T_a = gas.T
        a_a = math.sqrt(k_a*R_a*T_a)
    
    w_cr = math.sqrt(2*(h_cc-h_cr))                              #рассчитываю скорость потока (h0 = h + w2/2)
    F_specific_cr = v_cr/w_cr                                    #удельная площадь критики
    J_specific_vacuum_cr = w_cr + p_cr*F_specific_cr             #пустотный удельный импульс

    w_a = math.sqrt(2*(h_cc-h_a))                                #рассчитываю скорость потока (h0 = h + w2/2)
    F_specific_a = v_a/w_a                                       #удельная площадь среза сопла
    J_specific_vacuum_a = w_a + p_a*F_specific_a                 #пустотный удельный импульс

    result = np.zeros((14, 3))                                   #собираю результаты расета в двумерный массив
    result[:,0] = [lambda_cc,v_cc,p_cc,T_cc,h_cc,u_cc,k_cc,R_cc,cp_cc,cv_cc,a_cc,0,0,0]
    result[:,1] = [lambda_cr,v_cr,p_cr,T_cr,h_cr,u_cr,k_cr,R_cr,cp_cr,cv_cr,a_cr,w_cr,F_specific_cr,J_specific_vacuum_cr]
    result[:,2] = [lambda_a,v_a,p_a,T_a,h_a,u_a,k_a,R_a,cp_a,cv_a,a_a,w_a,F_specific_a,J_specific_vacuum_a]
    return result